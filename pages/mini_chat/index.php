<?

define('H', $_SERVER['DOCUMENT_ROOT'] . '/');
include_once H . 'sys/inc/start.php';
include_once H . 'sys/inc/compress.php';
include_once H . 'sys/inc/sess.php';
include_once H . 'sys/inc/settings.php';
include_once H . 'sys/inc/db_connect.php';
include_once H . 'sys/inc/ipua.php';
include_once H . 'sys/inc/fnc.php';
include_once H . 'sys/inc/user.php';
//подключаем языковой пакет
lang::start('mini_chat');
$set['title'] = lang('Мини чат');
include_once H . 'sys/inc/thead.php';
title();
aut();

if ($_SERVER['REQUEST_URI'] != '/pages/mini_chat/?refresh')
    $_SESSION['page_m'] = $_SERVER['REQUEST_URI'];
	
if (isset($_GET['refresh']))
    header("Location: " . $_SESSION['page_m']);


#--------------------------#
if (isset($_POST['msg']) && isset($user))
{
    $msg = $_POST['msg'];
    if (isset($_POST['translit']) and $_POST['translit'] == 1)
        $msg = translit($msg);
    $mat = antimat($msg);
    if ($mat)
        $err[] = lang('В тексте сообщения обнаружен мат:') . $mat;
    if (strlen2($msg) > $set['guest_max_post'])
        $err[] = lang('Сообщение слишком длинное');
    elseif (strlen2($msg) < 2/*or strlen2($msg) == null*/)
        $err[] = lang('Короткое сообщение');
    elseif (mysql_result(query("SELECT COUNT(*) FROM `guest` WHERE `id_user` = '".$user['id']."' AND `msg` = '" .
        my_esc($msg) . "' AND `time` > '" . (time() - 60) . "' LIMIT 1"), 0) != 0)
        $err = lang('Ваше сообщение повторяет предыдущее');
    else
        if (!isset($err))
        {
            #-------------------------------------#
            //вычисляем рейтинг за сообщение
            $rating_add_msg = rating::msgnum($msg);
            //сумируем
            $add_rating = ($rating_add_msg + 0.01);
            //добавляем рейтинг
            rating::add($user['id'], $add_rating, 1, null, 'mini_chat');
            #-------------------------------------#
            mysql_query("INSERT INTO `guest` (id_user, `time`, msg) VALUES('".$user['id']."', '".$time."', '" .
                my_esc($msg) . "')");
            mysql_query("UPDATE `user` SET `balls` = '" . ($user['balls'] + $set['balls_guest']) .
                "' WHERE `id` = '".$user['id']."' LIMIT 1");
            $_SESSION['message'] = lang('Сообщение успешно добавлено');
            exit(header("Location: ?"));
        }
} elseif (isset($_POST['msg']) && !isset($user) && isset($set['write_guest']) && 
$set['write_guest'] == 1 && isset($_SESSION['captcha']) && isset($_POST['chislo']))
{
    $msg = mysql_real_escape_string($_POST['msg']);
    
    if (isset($_POST['translit']) && $_POST['translit'] == 1)
        $msg = translit($msg);
    $mat = antimat($msg);
    if ($mat)
        $err[] =  lang('Обнаружен мат').': ' . $mat;
    if (strlen2($msg) > 1024)
    {
        $err =  lang('Сообщение слишком длинное');
    }
    elseif ($_SESSION['captcha'] != $_POST['chislo'])
    {
        $err = lang( 'Неверное проверочное число');
    }
    elseif (isset($_SESSION['antiflood']) && $_SESSION['antiflood'] > $time - 300)
    {
        $err =  lang('Для того чтобы чаще писать нужно авторизоваться');
    }
    elseif (strlen2($msg) < 2)
    {
        $err =  lang('Короткое сообщение');
    }
    if (mysql_result(mysql_query("
    SELECT COUNT(*) FROM `guest` 
    WHERE `id_user` = '".$user['id']."' 
    AND `msg` = '" . $msg . "' AND `time` > '" . (time() - 60) . "' LIMIT 1"), 0) != 0)
    {
        $err =  lang('Ваше сообщение повторяет предыдущее');
    }
    elseif (!isset($err))
    {
        $_SESSION['antiflood'] = $time;
        mysql_query("
        INSERT INTO `guest` (id_user, `time`, msg) 
        VALUES('0', '".time()."', '[i](IP:" . $_SERVER['REMOTE_ADDR']. ")[/i]
        " . $msg . "')");
        msg(lang('Успешно'));
    }
}
#-------------------------------------------#
err();
switch (empty($_GET['type']) ? false : $_GET['type'])
{
    default:
        if (user_access('guest_clear'))
            echo '<div class="guestbook"><a href="?type=deletmessages"> ' . lang('Полная очистка мини чата') .
                '</a></div>' . PHP_EOL;
        echo '<div class="guestbook"><a href="?type=who">' . lang('Кто в чате?') .
            '</a> :: <a href="?refresh">' . lang('Обновить') . '</a></div>';
        if (isset($user) || (isset($set['write_guest']) && $set['write_guest'] == 1))
        {
            panel_form::head();
            echo '<div class="guestbook">' . PHP_EOL;
            if (isset($user) || (isset($set['write_guest']) && $set['write_guest'] == 1 &&
                (!isset($_SESSION['antiflood']) || $_SESSION['antiflood'] < time() - 300)))
            {
                echo '<form method="post" name="message" action="?' . $passgen . '">' . PHP_EOL;
                echo lang('Сообщение') . ':<br /><textarea name="msg"></textarea><br />' . PHP_EOL;
                if (isset($user) && $user['set_translit'] == 1)
                    echo '<label><input type="checkbox" name="translit" value="1" /> ' . lang('Транслит') .
                        '</label><br />' . PHP_EOL;
                if (!isset($user))
                    echo '<img src="/captcha.php?SESS=' . $sess . '" width="100" height="30" alt="captcha" /><br />
                    <input name="chislo" size="5" maxlength="5" value="" type="text" /><br />' . PHP_EOL;
                echo '<input value="' . lang('Отправить') . '" type="submit" />' . PHP_EOL;
                echo '</form>' . PHP_EOL;
            } elseif (isset($set['write_guest']) && $set['write_guest'] == 1 && isset($_SESSION['antiflood']) &&
            $_SESSION['antiflood'] > time() - 300)
            {
                echo '<div class="foot">' . PHP_EOL;
                echo '* ' . lang('Гостем вы можете писать только по 1 сообщению в 5 минут') .
                    '<br />' . PHP_EOL;
                echo '</div>' . PHP_EOL;
            }
            echo '</div>' . PHP_EOL;
            panel_form::foot();
        }
        $k_post = mysql_result(query("SELECT COUNT(*) FROM `guest`"), 0);
        $k_page = k_page($k_post, $set['p_str']);
        $page = page($k_page);
        $start = $set['p_str'] * $page - $set['p_str'];
        echo '<table class="post">' . PHP_EOL;
        if ($k_post == 0)
            echo '<tr><td class="err"><span>' . lang('Тут никто, ничего, еще не писал') . '</span></td></tr>';
        $q = query("
SELECT a . * , b.id AS id_u, b.nick, b.pol, b.level, COUNT( c.id_user ) ban
FROM `guest` a
LEFT JOIN user b ON a.id_user = b.id
LEFT JOIN ban c ON b.id = c.id_user AND c.`time` > " . time() . "
GROUP BY a.id
ORDER BY a.id DESC LIMIT ".$start.", ".$set['p_str']);
        $z = 2;
        while ($post = mysql_fetch_assoc($q))
        {
		
            if ($post['id_user'] == 0)
            {
                $post['id_u'] = 0;
                $post['pol'] = 'guest';
                $post['level'] = 0;
            }   //else 
                //$ank = get_user($post['id_user']);
            if ($set['avatar_show'] == 1)
            {
                echo '<tr><td class="avar" style="" rowspan="2">' . PHP_EOL;
                avatar($post['id_u'], 90, 90);
                echo '</td>' . PHP_EOL;
            }
            echo '<tr><td class="z_'.($z % 2 ? 1 : 2).'">' . PHP_EOL;
			++$z;

            if ($post['id_u'] == 0)
                echo lang('Гость') . ' (' . vremja($post['time']) . ')' . PHP_EOL;
            else
                echo nick($post['id_u']) . ' (' . vremja($post['time']) . ') <br />' . PHP_EOL;
            echo '<span class="status" style="float:right;margin-top: -17px;">' . (user_access
                ('guest_delete') ? '<a href="?type=delete&amp;id=' . $post['id'] . '" class="trash" ></a>' : false) .
                ' </span>' . PHP_EOL;
            #-----------------------------------------#
            //Блокируем сообщение если человек в бане
            if (/*count::query('ban', "`id_user` = '" . $post['id_u'] . "' AND `time` > " . time()) == 0*/$post['ban'] == 0 && 
            $set['msg_ban_set'] == 1)
            {
                echo output_text($post['msg']);
                if ( isset($user) && $user['id'] != $post['id_user'] )
                {
                    echo '
		<br /><br />
		<span class="ank_span_m" ><a href="?type=otvet&amp;id=' . $post['id'] . '">' . lang('Ответить') .
                        ' </a></span>' . ($user['level'] < 1 && $ank['level'] < 1 ?
                        '<span class="ank_span_m">
		 <a href="?type=jaloba&amp;id=' . $post['id'] . '"> ' . lang('Жалоба') . '</a></span>' : false);
                }
            } 
			else
            {
                echo ($user['group_access'] >= 1 ? '<span style="color:red">' . $set['msg_ban'] .
                    '</span> : <br />' . output_text($post['msg']) : $set['msg_ban']);
            }
            #-----------------------------------------#
            echo '</td></tr>' . PHP_EOL;
			
        }
        echo '</table>';
        if ($k_page > 1)
            str('?', $k_page, $page);
        break;
    case 'otvet':
        if (!isset($user))
        {
            $_SESSION['message'] = lang('Доступ закрыт');
            exit(header('Location: ?'));
        }
        $q = query("
        SELECT A.*, B.id AS id_u, B.nick, B.group_access FROM `guest` A
        LEFT JOIN user B ON A.id_user = B.id
        WHERE A.id = " . intval($_GET['id']));
        $post = mysql_fetch_assoc($q);
            if ($post['id_user'] == 0)
            {
                $post['id_u'] = 0;
                $post['pol'] = 'guest';
                $post['nick'] = lang('Гость');
            }
        //$ank = get_user($post['id_user']);
        if ($user['id'] != $post['id_user'])
        {
            if (isset($_POST['save']))
            {
                $msg = $post['msg'] . '
--------
				' . mysql_real_escape_string($_POST['jaloba_msg']);
                mysql_query("
                INSERT INTO `guest` (`id_user` ,`time` ,`msg`)
                VALUES ('" . $user['id'] . "',  '" . time() . "',  '" . $msg . "');");
                if ($post['id_u'] != $user['id'] && $post['id_u'] !== 0)// у гостя нет журнала
                    mysql_query("
                    INSERT INTO `jurnal` (`id_user`, `id_kont`, `msg`, `time`, `type`) 
                    VALUES('0', '".$post['id_u']."', '".$user['nick']." Ответил на ваше сообщение 
                    " . $msg . " [url=/pages/mini_chat/?type=post&id=".$post['id']."]Подробнее[/url]', '" . time() . "', 'my_chat')");
                $_SESSION['message'] = lang('Успешно');
                exit(header('Location: ?'));
            }
        }
        echo '
	<div class="p_m">' . PHP_EOL . '
	<!--div class="status_o_s"> </div-->
	<div class="status_o">' . PHP_EOL . ' 
	' . output_text($post['msg']) . '
	</div>
	<br />' . PHP_EOL . '
	<span class="ank_span_m"><a href="?type=otvet&amp;id=' . $post['id'] . '"> ' . lang('Ответ') .
            '</a></span>
	' . ($post['group_access'] <= 1 ?
            '<span class="ank_span_m"><a href="?type=jaloba&amp;id=' . $post['id'] . '"> ' . lang('Жалоба') .
            '</a></span>' : false) . '
	<span class="ank_span_m"><a href="?"> ' . lang('Назад') . '</a></span>
	</div>'. PHP_EOL;
        echo '<div class="guestbook">' . PHP_EOL;
        echo '<form method="post" action="">' . PHP_EOL;
        echo lang('Ответ') . ':<br /><textarea name="jaloba_msg">' . $post['nick'] .
            ', </textarea><br />' . PHP_EOL;
        if (isset($user) && $user['set_translit'] == 1)
            echo '<label><input type="checkbox" name="translit" value="1" /> ' . lang('Транслит') .
                '</label><br />' . PHP_EOL;
        echo '
<input value="' . lang('Отправить') . '" type="submit"  name="save"/>' . PHP_EOL . '
<span class="ank_span_m"><a href="?type=post&amp;id=' . $post['id'] . '"> ' . lang('Отмена') .
            '</a></span>' . PHP_EOL;
        echo '</form></div>' . PHP_EOL;
        break;
    case 'post':
        $q = query("SELECT id, msg FROM `guest` WHERE id = " . intval($_GET['id']));
        $post = mysql_fetch_assoc($q);
        echo '
	<div class="p_m">' . PHP_EOL . '
	<!--div class="status_o_s"> </div-->
	<div class="status_o">' . PHP_EOL . output_text($post['msg']) . '
	</div>
	<br />' . PHP_EOL . '
	<span class="ank_span_m"><a href="?type=otvet&amp;id=' . $post['id'] . '"> ' . lang('Ответ') .
            '</a></span>
	' . ($user['level'] < 1 ? '<span class="ank_span_m"><a href="?type=jaloba&amp;id=' . $post['id'] . '"> ' .
            lang('Жалоба') . '</a></span>' : false) . '
	<span class="ank_span_m"><a href="?"> ' . lang('Назад') . '</a></span>
	</div>' . PHP_EOL;
        break;
    case 'jaloba':
        if (!isset($user))
        {
            $_SESSION['message'] = lang('Доступ закрыт');
            exit(header('Location: ?'));
        }
        $q = query("SELECT * FROM `guest` WHERE id = " . intval($_GET['id']));
        $post = mysql_fetch_assoc($q);
        if ($user['id'] != $post['id_user'])
        {
            if (isset($_POST['save']))
            {
                $msg = mysql_real_escape_string($_POST['jaloba_msg']);
                $prich = lang('Категория') . ' = ' . mysql_real_escape_string($_POST['prich']);
                $msg .= ' ' . lang('Подана жалоба на сообщение') . '
[url=/pages/mini_chat/?type=post&id=' . intval($_GET['id']) . ']' . $post['msg'] . '[/url]';
                mysql_query("
                INSERT INTO `jurnal_system` (`time` ,`type` ,`read` ,`id_user`,`msg`,`id_kont`)
                VALUES ( '" . time() . "',  'spam_chat',  '0',  '" . $post['id_user'] . "','" .
                    $msg . ' ' . $prich . "','" . $user['id'] . "');");
                $_SESSION['message'] = lang('Успешно');
                exit(header('Location: ?'));
            }
            echo '
	<div class="p_m">' . PHP_EOL . 
    lang('Вы пытаетесь подать жалобу на сообщение') . ' : <br />
	<!--div class="status_o_s"> </div-->
	<div class="status_o">' . PHP_EOL . 
    output_text($post['msg']) . '
	</div>
	</div>' . PHP_EOL;
            echo '<div class="guestbook">' . PHP_EOL;
            echo '<form method="post" action="">' . PHP_EOL;
            echo lang('Комментарий') .
                ':<br /><textarea name="jaloba_msg"></textarea><br />' . PHP_EOL;
            if (isset($user) && $user['set_translit'] == 1)
                echo '<label><input type="checkbox" name="translit" value="1" /> ' . lang('Транслит') .
                    '</label><br />' . PHP_EOL;
            echo lang('Категория') . ' :<select name="prich">';
            echo '<option value="' . lang('Мат') . '"> ' . lang('Мат') . '</option>';
            echo '<option value="' . lang('Оскорбления') . '"> ' . lang('Оскорбления') .
                '</option>';
            echo '<option value="' . lang('Спам') . '">' . lang('Спам') . ' </option>';
            echo '<option value="' . lang('Мошенничество') . '"> ' . lang('Мошенничество') .
                '</option>';
            echo '<option value="' . lang('Флуд,офтоп,тролинг') . '"> ' . lang('Флуд,офтоп,тролинг') .
                '</option>';
            echo '<option value="' . lang('Другое') . '"> ' . lang('Другое') . '</option>';
            echo '</select><br />' . PHP_EOL;
            echo lang('Вы уверены ?') . '
<input value="' . lang('Да') . '" type="submit"  name="save"/>
<span class="ank_span_m"><a href="?"> ' . lang('Нет') . '</a></span>
</form>' . PHP_EOL;
            echo '</div>' . PHP_EOL;
        } else
        {
            $_SESSION['message'] = lang('Нельзя подавать жалобы на свои сообщения');
            exit(header('Location: ?'));
        }
        break;
    case 'who':
        #----------------------------------------------------------#
        echo '<div class="foot"><a href="?">' . lang('Назад') . '  </a></div>' . PHP_EOL;
        $k_post = mysql_result(query("SELECT COUNT(*) FROM `user` WHERE `date_last` > '" .
            (time() - 300) . "' AND `url` like '/pages/mini_chat/%'"), 0);
        $k_page = k_page($k_post, $set['p_str']);
        $page = page($k_page);
        $start = $set['p_str'] * $page - $set['p_str'];
        $q = query("
        SELECT * FROM `user` WHERE `date_last` > '" . (time() - 300) . "' 
        AND `url` like '/pages/mini_chat/%' 
        ORDER BY `date_last` DESC LIMIT ".$start.", ".$set['p_str']);
        if ($k_post == 0)
            msg(lang('Не кого нету'));
        while ($guest = mysql_fetch_assoc($q))
        {
            echo '  <div class="p_m">' . PHP_EOL;
            echo nick($guest['id']);
            echo '</div>' . PHP_EOL;
        }
        if ($k_page > 1)
            str("?", $k_page, $page);
        #----------------------------------------------------------#
        break;
    case 'deletmessages':
        if (user_access('guest_clear'))
        {
            if (isset($_GET['deletyes']))
            {
                $q_db = query("SELECT id FROM `guest`");
                $del_count = 0;
                while ($post = mysql_fetch_assoc($q_db))
                {
                    mysql_query("DELETE FROM `guest` WHERE `id` = '".$post['id']."'");
                    $del_count++;
                }
                $md = ('Удалено') . ' ' . $del_count . ' ' . ('сообщений');
                admin_log(('Мини чат'), ('Полная очистка'), $md);
                mysql_query("OPTIMIZE TABLE `guest`");
                msg(lang('Удалено') . ' ' . lang('сообщений') . ' ' . $del_count);
                echo "<div class='foot'><a href='?'>" . lang('Вернуться в мини чат') .
                    " </a><br /></div>";
            }
            if (!isset($_GET['deletyes']))
            {
                echo '<form method="post" class="foot" action="?type=deletmessages&amp;deletyes">' . PHP_EOL;
                echo lang('Удалить все сообщения') . '? <br />
* <span style="color:red;">' . lang('Будут удалены все сообщения') .
                    ' </span><br />' . PHP_EOL;
                echo '<input value="' . lang('Согласен на очистку') . '" type="submit" /><br />' . PHP_EOL;
                echo '<a href="?">' . lang('Отказываюсь') . '</a><br />';
                echo '</form>' . PHP_EOL;
            }
        } else
            msg(lang('Доступ закрыт'));
        break;
        break;
    case 'delete':
        if (user_access('guest_delete'))
        {
            $post = mysql_fetch_assoc(query("
            SELECT a.*, b.id AS id_u, b.nick, b.level, b.pol FROM `guest` a 
            LEFT JOIN user b ON a.id_user = b.id
            WHERE a.`id` = '" . intval($_GET['id']) . "' LIMIT 1"));
            if ($post['id_user'] == 0)
            {
                $post['id_u'] = 0;
                $post['pol'] = 'guest';
                $post['level'] = 0;
                $post['nick'] = lang('Гость');
            } //else
                //$ank = get_user($post['id_user']);
            $md = lang('Удаление сообщения от') . ' ' . $post['nick'];
            admin_log(lang('Мини чат'), lang('Удаление сообщения'), $md);
            mysql_query("DELETE FROM `guest` WHERE `id` = '".$post['id']."'");
            $_SESSION['message'] = lang('сообщение удалено');
            exit(header("Location: ?"));
        }
        break;
}
include_once H . 'sys/inc/tfoot.php';
